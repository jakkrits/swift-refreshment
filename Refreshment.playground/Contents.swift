//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
let width = 93
let length =  7
print(str, "\(width)")
print("type cast ", String(width + length))

//Optionals
let mightBeInt: Int? = 9
print(mightBeInt)
if let myInt = mightBeInt {
    print(myInt)
}

var someText: String? = "Hello Dude!"
if let myText = someText , myText.hasPrefix("H"), let name = someText {
    print(myText, name)
}

//Array 
var ratingList = ["apple", "pineapple", "orange", "banana"]
ratingList[1] = "กระบวย"
print(ratingList)
ratingList.insert("แตร", at: 4)
print(ratingList)


//Switch
let vegetable = "red pepper"
var veggieComment = ""
switch vegetable {
case "celery":
     veggieComment = "เซเลรี่"
case "cucumber", "watercres":
     veggieComment = "ok that'd be great"
case let x where x.hasSuffix("pepper"):
     veggieComment = "SPICY!!!"
default:
     veggieComment = "เยสเปด เอนเตอร์ ซั๊มติ่ง"
}
print(veggieComment)

//Function
func greet(name: String, day: String) -> String {
    return "Hello \(name)!!, Today is \(day)"
}

print(greet(name: "John", day: "Monday"))

let message = greet(name: "May", day: "Tuesday")
print(message)

//Class
class Shape {
    var numberOfSides = 0
    func simpleDescription() -> String {
        return "A shape with \(numberOfSides) sides."
    }
}
var shape = Shape()
shape.numberOfSides = 4
var shapeDescription = shape.simpleDescription()

class NamedShape {
    var numberOfSides = 0
    var name: String
    init(name: String) {
        self.name = name
    }
    func simpleDescription() -> String {
        return "This \(name) has \(numberOfSides) sides"
    }
}
var myTri = NamedShape(name: "Triangle")
myTri.numberOfSides = 3
myTri.simpleDescription()

class Square: NamedShape {
    var sideLength: Double
    
    init(sideLength: Double, name: String) {
        self.sideLength = sideLength
        super.init(name: name)
        numberOfSides = 4
    }
    
    func area() -> Double {
        return sideLength * sideLength
    }
    
    override func simpleDescription() -> String {
        let area = self.area()
        return "The square with length of \(sideLength) has an area of \(area)"
    }
}
var mySquare = Square(sideLength: 12.12, name: "MySquare")
print(mySquare.simpleDescription())


//Failable Initializer
class Circle: NamedShape {
    var radius: Double
    init?(radius: Double, name: String) {
        self.radius = radius
        super.init(name: name)
        numberOfSides = 1
        if radius <= 0 {
            return nil
        }
    }
    override func simpleDescription() -> String {
        return "A circle with radius of \(radius)"
    }
}
var myCircle = Circle(radius: 0, name: "My Circle")
myCircle?.simpleDescription()

class Triangle: NamedShape {
    init(sideLength: Double, name: String) {
        super.init(name: name)
        numberOfSides = 3
    }
}

let shapesArray = [Triangle(sideLength: 1.5, name: "triangle1"), Triangle(sideLength: 4.2, name: "triangle2"), Square(sideLength: 3.2, name: "square1"), Square(sideLength: 2.7, name: "square2")]
var squares = 0
var triangles = 0
for shape in shapesArray {
    if let square = shape as? Square {
        squares += 1
    } else if let triangle = shape as? Triangle {
        triangles += 1
    }
}
print("\(squares) squares and \(triangles) triangles.")

//Enumerations and Structures

enum Rank: Int {
    case Ace = 1
    case Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten
    case Jack, Queen, King
    func simpleDescription() -> String {
        switch self {
        case .Ace:
            return "ace"
        case .Jack:
            return "jack"
        case .Queen:
            return "queen"
        case .King:
            return "king"
        default:
            return String(self.rawValue)
        }
    }
}

let ace = Rank.Ace
let five = Rank.Five
print(ace.simpleDescription(), five.simpleDescription())

if let ace = Rank(rawValue: 1) {
    print(ace.simpleDescription())
}

enum Suit {
    case Spades, Hearts, Diamonds, Clubs
    func simpleDescription() -> String {
        switch self {
        case .Spades:
            return "spades"
        case .Hearts:
            return "hearts"
        case .Diamonds:
            return "diamonds"
        case .Clubs:
            return "clubs"
        }
    }
}
let hearts = Suit.Hearts
let heartsDescription = hearts.simpleDescription()

struct Card {
    var rank: Rank
    var suit: Suit
    func simpleDescription() -> String {
        return "The \(rank.simpleDescription()) of \(suit.simpleDescription())"
    }
}
let threeOfSpades = Card(rank: .Three, suit: .Spades)
let threeOfSpadesDescription = threeOfSpades.simpleDescription()


//Protocol
protocol ExampleProtocol {
    var simpleDescription: String { get }
    func adjust()
}

class ExampleClass: ExampleProtocol {
    var simpleDescription: String = "A very simple class."
    var anotherProperty: Int = 69105
    func adjust() {
        simpleDescription += "..NOW conforms to proptocol"
    }
}
let a = ExampleClass()
a.adjust()
let aDescription = a.simpleDescription
print(aDescription)

class SimpleClass2: ExampleProtocol {
    var simpleDescription: String = "Another very simple class."
    func adjust() {
        simpleDescription += "  Adjusted."
    }
}

var protocolArray: [ExampleProtocol] = [ExampleClass(), ExampleClass(), SimpleClass2()]
for instance in protocolArray {
    instance.adjust()
}
protocolArray


//UIKit
let redSquare = UIView(frame: CGRect(x: 2, y: 2, width: 44, height: 44))
redSquare.backgroundColor = UIColor.red























